import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class ContaService {
  collection: AngularFirestoreCollection;

  constructor(
    private db: AngularFirestore

  ) { }

  registraConta(conta) {
    conta.id = this.db.createId();
    this.collection = this.db.collection('conta');
    return this.collection.doc(conta.id).set(conta);
  }

  lista(tipo) {
    this.collection = this.db.collection('conta', ref => ref.where('tipo', '==', tipo));
    return this.collection.valueChanges();
  }

  remove(conta) {
    this.collection = this.db.collection('conta');
    this.collection.doc(conta.id).delete();
  }
}
