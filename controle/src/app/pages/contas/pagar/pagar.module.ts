import { NgModule } from '@angular/core';
import { CommonModule, NgForOf } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule, IonIcon, IonItem, IonLabel, IonList } from '@ionic/angular';

import { PagarPageRoutingModule } from './pagar-routing.module';

import { PagarPage } from './pagar.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonLabel,
    IonList,
    IonIcon,
    IonItem,
    IonicModule, PagarPageRoutingModule
  ],
  declarations: [PagarPage]
})
export class PagarPageModule { }
