// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyBn4wMvNcaHwG4edFOUXgvXfOfOh1I88XY',
    authDomain: 'controle-gu3001687.firebaseapp.com',
    databaseURL: 'https://controle-gu3001687.firebaseio.com',
    projectId: 'controle-gu3001687',
    storageBucket: 'controle-gu3001687.appspot.com',
    messagingSenderId: '304176403438',
    appId: '1:304176403438:web:5905b3242a6b61917c34ff',
    measurementId: 'G-J742NLE1CC'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
